package main

import (
	"log"
	"net/http"
	"fmt"
	"github.com/gorilla/websocket"
)

// Read and Write buffer size for our WebSocket connection
var upgrader = websocket.Upgrader{
    ReadBufferSize:  1024,
    WriteBufferSize: 1024,
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "HomePage")
}

func wsEndpoint(w http.ResponseWriter, r *http.Request) {
	// determine whether or not an incoming request from a different domain is allowed to connect, and if it isn’t they’ll be hit with a CORS error.
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	
	// UPGRADE THIS CONNECTION TO A WEBSOCKET CONNECTION!!!
    ws, err := upgrader.Upgrade(w, r, nil)
    if err != nil {
        log.Println(err)
	}
	
	// helpful log statement to show connections
    log.Println("Client Connected")

	// In order to send messages from our Go application to any connected WebSocket clients
	err = ws.WriteMessage(1, []byte("Hi Client!"))
    if err != nil {
        log.Println(err)
	}
	
	// listen indefinitely for new messages coming through on our WebSocket connection
    reader(ws)
}

// will take in a pointer to the WebSocket connection that we received from our call to upgrader.Upgrade:
// define a reader which will listen for new messages being sent to our WebSocket endpoint
func reader(conn *websocket.Conn) {
	for {
	    // read in a message
        messageType, p, err := conn.ReadMessage()
        if err != nil {
            log.Println(err)
            return
        }
    	// print out that message for clarity
        fmt.Println("CLIENT MESSAGE: "+string(p))

        if err := conn.WriteMessage(messageType, p); err != nil {
            log.Println(err)
            return
        }

    }
}

func setupRoutes() {
	http.HandleFunc("/", homePage)
	http.HandleFunc("/ws", wsEndpoint)
}

func main() {
	fmt.Println("Hello Word")
	setupRoutes()
	log.Fatal(http.ListenAndServe(":8080", nil))
}