# websocket-server-go-gorilla

WebSocket del lado del servidor

## Conceptos a desarrollar

WebSocket

## Tecnologías

Lenguaje: Golang

Paquete utilizado para la implementación: github.com/gorilla/websocket

## Uso

Para levantar el servidor ejecutar el siguiente comando:

```bash
1) go run main.go
```
## Endpoints -  descripción

```bash
1) /ws: transmite lo que recibe
```

## Ejemplo de WebSocket Client
[websocket-client](https://gitlab.com/g-javascript/websocket-client)

## Referencias
[Elliot Forbes](https://tutorialedge.net/golang/go-websocket-tutorial/)
